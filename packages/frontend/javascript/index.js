import "../stylus/index.styl";
import "../stylus/reset.styl";

// Note that a dynamic `import` statement here is required due to
// webpack/webpack#6615, but in theory `import { greet } from './pkg';`
// will work here one day as well!
const rust = import("../../../build/packages/frontend/wasm");

rust.then((m) => m.main()).catch(console.error);
