use std::cell::RefCell;

use super::tx::Tx;

type RxHandler<T> = Box<dyn Fn(T)>;

pub struct Rx<T> {
	handler: RefCell<Option<RxHandler<T>>>,
}
impl<T> Rx<T> {
	pub fn new() -> Self {
		Self {
			handler: RefCell::new(None),
		}
	}

	pub fn set_handler(&self, handler: Box<dyn Fn(T)>) {
		*self.handler.borrow_mut() = Some(handler);
	}
}

impl<T> Tx for Rx<T> {
	type Item = T;

	fn send(&self, value: Self::Item) {
		if let Some(handler) = &*self.handler.borrow() {
			handler(value)
		}
	}
}

#[cfg(test)]
mod tests {
	use crate::reactive::{rx::Rx, tx::Tx};
	use std::{cell::RefCell, rc::Rc};

	#[test]
	fn simple() {
		let messages = Rc::new(RefCell::new(Vec::new()));
		let rx = Rx::new();

		rx.send(1);
		rx.set_handler(Box::new({
			let messages = messages.clone();
			move |value| messages.borrow_mut().push(value)
		}));
		assert_eq!(&*messages.borrow(), &[]);

		rx.send(2);
		assert_eq!(&*messages.borrow(), &[2]);
	}
}
