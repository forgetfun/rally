const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const webpack = require("webpack");
const WasmPackPlugin = require("@wasm-tool/wasm-pack-plugin");

module.exports = {
	entry: "./javascript/index.js",
	output: {
		path: path.resolve(__dirname, "../../build/packages/frontend/webpack"),
		filename: "index.js",
	},
	plugins: [
		new HtmlWebpackPlugin({
			title: "rally",
		}),
		new WasmPackPlugin({
			crateDirectory: path.resolve(__dirname, "."),
			outDir: "../../build/packages/frontend/wasm",
		}),

		// have this example work in Edge which doesn't ship `TextEncoder` or
		// `TextDecoder` at this time.
		new webpack.ProvidePlugin({
			TextDecoder: ["text-encoding", "TextDecoder"],
			TextEncoder: ["text-encoding", "TextEncoder"],
		}),
	],
	mode: "development",
	experiments: {
		asyncWebAssembly: true,
	},
	module: {
		rules: [
			{
				test: /\.styl$/,
				use: [
					{
						loader: "style-loader", // creates style nodes from JS strings
					},
					{
						loader: "css-loader", // translates CSS into CommonJS
					},
					{
						loader: "stylus-loader", // compiles Stylus to CSS
					},
				],
			},
		],
	},
};
