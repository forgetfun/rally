///////////
// lints //
///////////
#![deny(unsafe_code)]
// warnings are ok during development, but have to be explicitly allowed at usage site if required
// during release #![cfg_attr(not(debug_assertions), deny(warnings))]
#![warn(clippy::pedantic)]
#![warn(clippy::nursery)]
#![warn(clippy::cargo)]
#![allow(clippy::cargo_common_metadata)]
// this is triggered by external dependencies and can't be fixed by us
#![allow(clippy::multiple_crate_versions)]
// sorry, good documentation is yet to come
#![allow(clippy::missing_panics_doc)]

use ansi_term::Colour::Green;
use anyhow::Result;
use clap::Clap;
use duct::cmd;
use log::info;
use std::env::{self};

const LOG_ENV_VAR: &str = "RUST_LOG";

fn init() -> GlobalOpts {
	if env::var_os(LOG_ENV_VAR).is_none() {
		env::set_var(LOG_ENV_VAR, "info");
	}
	pretty_env_logger::init_custom_env(LOG_ENV_VAR);

	GlobalOpts::parse()
}

fn main() -> Result<()> {
	let opts = init();

	match opts.subcommand {
		GlobalSubcommand::Dev => {
			heading("starting dev server");
			cmd!("npm", "run", "dev").dir("packages/frontend").run()?;
		},
		GlobalSubcommand::Check => {
			heading("running clippy");
			cmd!("cargo", "clippy").run()?;
			heading("running tests");
			cmd!("cargo", "test").run()?;
		},
	}

	Ok(())
}

fn heading(message: &str) {
	info!(
		"{}",
		Green.bold().paint(format!(
			"{ruler} {message} {ruler}",
			message = message,
			ruler = "-".repeat(16)
		))
	);
}

#[derive(Clap)]
struct GlobalOpts {
	#[clap(subcommand)]
	subcommand: GlobalSubcommand,
}

#[derive(clap::Subcommand)]
enum GlobalSubcommand {
	/// start the dev server
	Dev,
	Check,
}
